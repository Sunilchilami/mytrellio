import { SignInService } from './../../services/signin.service';
import {Router} from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;  message;
  private alive:boolean;
  username;
  uid;
  constructor(private formBuilder: FormBuilder,private signInService:SignInService,private  router:Router) { }

  ngOnInit() {
      this.alive = true;
      this.loginForm = this.formBuilder.group({
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required, Validators.minLength(6)]]
      });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  userAuthentication() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }
      else{
          console.log('hii');
          this.signInService.userAuthentication({...this.loginForm.value,created_by_fk:'1'})
          .pipe(takeWhile(()=>this.alive))
          .subscribe((data)=> {
              console.log(data);
              this.message = data.status;
              console.log(this.message);
              localStorage.setItem('key', data['data'][0].firstName);
              localStorage.setItem('key1', data['data'][0].id);
              this.username = localStorage.getItem('key');
              this.uid = localStorage.getItem('key1');
              console.log('session uid'+this.uid);
              console.log('session user'+this.username);
              if(data.status==="success"){
                this.router.navigateByUrl('/home/'+ data['data'][0].id);
              }
          
         const alertMessage = {
             message: data.message,
             status: data.status,
            
         };
      });
      }

     }

 ngOnDestroy(){
     this.alive = false;
 }

  
}