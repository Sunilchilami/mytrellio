
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import {HeaderComponent} from './header/header.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, } from '@angular/common/http';
import { BoardComponent } from './board/board.component';
import { DragDropModule } from '@angular/cdk/drag-drop';


const routes: Routes = [
   {path:'home/:id', component:HomeComponent},
  {path:'login' , component:LoginComponent},
  {path:'signup', component:SignupComponent},
  {path:'board/:id',component:BoardComponent},
  {path: '', redirectTo: 'login', pathMatch: 'full'}

];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,LoginComponent,SignupComponent,HeaderComponent, BoardComponent, 
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    DragDropModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
