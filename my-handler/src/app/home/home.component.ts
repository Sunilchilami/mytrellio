import { ActivatedRoute } from '@angular/router';
import { HomeService } from './../../services/home.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  boardForm: FormGroup;
  submitted = false;
  private alive:boolean;
  boardDetails:any; id; uid;
  constructor(private formBuilder: FormBuilder,private homeService:HomeService,private route:ActivatedRoute) { }

  ngOnInit() {
      this.alive = true;

      this.id = this.route.params.subscribe(params => {
        this.uid = +params.id;
      });

      this.boardForm = this.formBuilder.group({
        hname: ['',[ Validators.required,Validators.maxLength(100)]],
    });
      this.getBoards(this.uid);
  }

  // // convenience getter for easy access to form fields
   get f() { return this.boardForm.controls; }

  saveBoard() {
    console.log('saveBoard errr inside');
      this.submitted = true;

      // stop here if form is invalid
      if (this.boardForm.invalid) {
          return;
      }
      else{
          console.log('saveBoard inside');
          console.log(this.boardForm.value);
          this.homeService.saveBoard({...this.boardForm.value,created_by_fk:this.uid})
          .pipe(takeWhile(()=>this.alive))
          .subscribe((data)=> {
              console.log(data.message);
         const alertMessage = {
             message: data.message,
             status: data.status
         };
         this.getBoards(this.uid);
      });
      }

     }

     getBoards(uid){
         
       this.homeService.getBoard(this.uid)
       .pipe(takeWhile(()=>this.alive))
       .subscribe((data)=> {
           console.log(data.message);
         this.boardDetails = data['data']
      });
     }

 ngOnDestroy(){
     this.alive = false;
 }

  
}