import { ActivatedRoute, Router } from '@angular/router';
import { SignInService } from './../../services/signin.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from './must-match';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit,OnDestroy {
  registerForm: FormGroup;
    submitted = false;
    message;
    private alive:boolean;
    constructor(private formBuilder: FormBuilder,private signInService:SignInService,private  router:Router) { }

    ngOnInit() {
        this.alive = true;
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmPassword: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmPassword')
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    createUser() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
        else{
            console.log('hii');
            this.signInService.createUser({...this.registerForm.value,created_by_fk:'1'})
            .pipe(takeWhile(()=>this.alive))
            .subscribe((data)=> {
                console.log(data.message);
                if(data.status==="SUCCESS"){
                  this.router.navigateByUrl('/login');
                }
              this.message = data.status;
           const alertMessage = {
               message: data.message,
               status: data.status
           };
        });
        }
  
       }

   ngOnDestroy(){
       this.alive = false;
   }

    
}