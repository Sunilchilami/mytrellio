import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  username;
  constructor(private router:Router) { }

  ngOnInit() {

    this.username = localStorage.getItem('key');
    console.log('session user inn header'+this.username);
  }

  logout(){
    window.localStorage.clear();
    this.router.navigateByUrl('/login');
  }

}
