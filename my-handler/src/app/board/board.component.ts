import { element } from 'protractor';
import { HttpClient } from '@angular/common/http';
import { BoardService } from './../../services/board.service';
import { HomeService } from './../../services/home.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit,OnDestroy {
  taskForm: FormGroup;
  submitted = false;
  eventArray = [];
  eventName;
  private alive:boolean;
  taskDetails:any;
  boardId:number= 0;
  sub:any;
  task_id;
  uid;
  fname;
  eid;
openTxtBox = false;
todo=[];
doing=[];
username;
done=[];
updatemode = false;
savemode ;
liveResult = [];
  constructor(private httpClient:HttpClient,private formBuilder: FormBuilder,private boardService:BoardService,private route: ActivatedRoute) { }

  ngOnInit() {
      this.alive = true;


      const curDate=new Date();
      console.log(curDate);

      
      this.sub = this.route.params.subscribe(params => {
        this.boardId = +params['id'];
      });
     
      this.taskForm = this.formBuilder.group({
        taskName: ['',[ Validators.required,Validators.maxLength(45)]],
    });

    this.username = localStorage.getItem('key');
    this.fname = this.username[0];
    // console.log('session user  '+this.username[0]);

      this.getTasks(this.boardId);
  }

  openTextbox(){
    this.savemode = true;
    this.openTxtBox = true; 
  }

  closeTextbox(){
    this.openTxtBox = false;
    this.savemode = false;
  }


  
  saveTask() {
    console.log('saveTask');
      this.submitted = true;
      this.savemode = false;
      // stop here if form is invalid
      if (this.taskForm.invalid) {
          return;
      }
      else{
        
        this.openTxtBox = false;
          console.log('saveTask inside');
          this.boardService.saveTask({...this.taskForm.value,created_by_fk:'1',boardId:this.boardId,event_id_fk:1})
          .pipe(takeWhile(()=>this.alive))
          .subscribe((data)=> {
              console.log(data.message);
         const alertMessage = {
             message: data.message,
             status: data.status
         };
         this.taskForm.reset();
         this.getTasks(this.boardId);
      });
      } 

     }

     getById(taskID){
       this.savemode = false;
     this.updatemode = true;
      this.openTxtBox = true;
      this.task_id = taskID;
       console.log(this.task_id);
       this.boardService.getById(taskID)
       .pipe(takeWhile(()=>this.alive))
       .subscribe((data)=> {
           console.log(data);
         const taskDetails = data['data'][0]
    this.taskForm.setValue({
      taskName : taskDetails.taskName
    })
         this.eventName = data['data'][0].hname;
        });
     }

     getTasks(boardId){
         console.log('inside gettask'+boardId);
       this.boardService.getTasks(boardId)
       .pipe(takeWhile(()=>this.alive))
       .subscribe((data)=> {
           console.log(data);
           this.todo = data['data'];
         this.eventName = data['data'][0].hname;
        //  this.event_id_fk = data['data'][0].event_id_fk;
console.log(this.eventName);
// for(var i=0;i<data.data.length;i++){
//   if(data['data'][i].event_id_fk===1){
//     this.todo = data['data'];
//     console.log(this.todo);
//   }
//   if(data['data'][i].event_id_fk===2){
//     this.doing = data['data'];
//     console.log(this.doing);
//   }

//   if(data['data'][i].event_id_fk===3){
//     this.done = data['data'];
//     console.log(this.done);
//   }
// }

         
      });
     }


     updateTask(){
      this.updatemode = false;
      this.openTxtBox = false;
       this.boardService.updateTask(this.task_id,{...this.taskForm.value});
     }
     
 ngOnDestroy(){
     this.alive = false;
 }


drop(event: CdkDragDrop<string[]>) {
 const arr = [];

  console.log(event);
  const eventData = event.container.data.indexOf[0];
  arr.push(eventData);
  const container_id = event.container.id;
  console.log(container_id);
    console.log(arr);
  //console.log(event.container.element.nativeElement);
 //   console.log(event.container.element.nativeElement);

   
   // this.httpClient.post<{status:string,message:string}>('http://localhost:9999/api/updateEvent', container_id)
  if (event.previousContainer === event.container) {
    moveItemInArray(event.container.data, event.previousIndex, event.currentIndex)
  } else {
    
    transferArrayItem(event.previousContainer.data,
      
                      event.container.data,
                      event.previousIndex,
                      event.currentIndex);
                      this.eventArray.push(event.container.data[0])
                      this.eid =  this.eventArray[0].id;

                      console.log(this.eid);
                      this.updateEvent(container_id,this.eid);
   
                    }
}

updateEvent(container_id,eid)
{
  console.log(this.eid);
  this.uid = localStorage.getItem('key1');
  console.log('session uid'+this.uid);
  this.boardService.updateEvent(container_id,this.eid,this.uid)
  .pipe(takeWhile(()=>this.alive))
  .subscribe((data)=>{
     console.log(data.status);
if(data.status === "updated")
{ 
 
  if(container_id==1){
    // console.log(container_id);
    const obj = "you have moved to todo again..";
    this.liveResult.push(obj);
  }
  if(container_id==2){ 
    //  console.log(container_id);
    const obj = "great..you have moved to doing..";
    this.liveResult.push(obj);
  }
  if(container_id==3){  console.log(container_id);
    const obj = "congrats ..you have done..";
    this.liveResult.push(obj);
  }
  // this.getTasks(this.boardId);

  // if(this.liveResult.length===10){
  //   this.liveResult.pop();
  // }
}
  })
}

}