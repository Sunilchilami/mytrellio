import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestAPIService } from './restApiService';

@Injectable({providedIn: 'root'})
export class BoardService {
  constructor(private http: HttpClient,private restApi:RestAPIService) { }

  saveTask(values:any){
    console.log(values);
   return this.http.post<{status:string,message:string}>('http://localhost:9997/api/saveTask', values);
  }//createUser

  getTasks(boardId:number){
    
    return this.http.get<{status:string,message:string,data:any}>('http://localhost:9997/api/getTasks/'+boardId);
  }//getTasks

  updateEvent(containerId,eid,updated_by_fk){
    console.log(containerId,eid,updated_by_fk);
    const obj = {containerId:containerId,updated_by_fk:updated_by_fk}
    return this.http.post<{status:string,message:string,data:any}>('http://localhost:9997/api/updateEvents/'+eid,obj);
  }

  getById(taskId:number){
    
    return this.http.get<{status:string,message:string,data:any}>('http://localhost:9997/api/getById/'+taskId);
  }


 

  updateTask(taskId,taskName){
    console.log(taskId,taskName);

    const obj = {"taskName":taskName}

    return this.http.post<{status:string,message:string,data:any}>('http://localhost:9997/api/updateTask/'+taskId,obj);
  }

}