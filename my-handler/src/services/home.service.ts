import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestAPIService } from './restApiService';

@Injectable({providedIn: 'root'})
export class HomeService {
  constructor(private http: HttpClient,private restApi:RestAPIService) { }

  saveBoard(values:any){
    console.log(values);
   return this.http.post<{status:string,message:string}>('http://localhost:9997/api/saveHandler', values);
  }//createUser

  getBoard(uid:number){
    
    return this.http.get<{status:string,message:string,data:any}>('http://localhost:9997/api/getHandler/'+uid);
  }//getHandler

}