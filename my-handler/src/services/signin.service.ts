import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestAPIService } from './restApiService';

@Injectable({providedIn: 'root'})
export class SignInService {
  constructor(private http: HttpClient,private restApi:RestAPIService) { }

  createUser(values:any){
    console.log(values);
   return this.http.post<{status:string,message:string}>('http://localhost:9997/api/createUser', values);
  }//createUser

  userAuthentication(values:any){
    console.log(values);
    return this.http.post<{status:string,message:string}>('http://localhost:9997/api/userAuth', values);
  }//userAuthentication

}