
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class RestAPIService {
API_URL  =  'http://localhost:9997/api/';
constructor(private  httpClient:  HttpClient) {}

 getApiUrl(){
     return  this.API_URL;
 }
}