const http=require('http');
const express=require('express');
const parser=require('body-parser');
const status = require('./db/db');
const signup = require('./signup');
const handler = require('./handler');
const tasks = require('./tasks');
const updateEvent = require('./updateEvent');

var app=express();
var port=9997;


app.use((req,resp,next)=>{
    resp.setHeader('Access-Control-Allow-Origin',"*");
    resp.setHeader('Access-Control-Allow-Headers',"Origin,X-Requested-With, Content-Type,Accept");
    resp.setHeader('Access-Control-Allow-Methods',"GET,POST,PATCH, DELETE, OPTIONS");
    next();
});

app.use(parser.json());

app.set('port',port);
const server=http.createServer(app);

server.listen(port,()=>{
    console.log('chaalo aytri '+ port);
});

app.post('/api/createUser',(req,resp,next)=>{

    signup.createUser(req,resp);
});

app.post('/api/userAuth',(req,resp,next)=>{
     signup.userAuth(req,resp);
});



app.post('/api/saveTask',(req,resp,next)=>{

    tasks.saveTask(req,resp);
});

app.get('/api/getTasks/:boardId',(req,resp,next)=>{
     tasks.getTasks(req,resp);
});

app.post('/api/updateTask/:taskId',(req,resp)=>{
  tasks.updateTask(req,resp);
});

app.get('/api/getEvents',(req,resp,next)=>{
     tasks.getEvents(req,resp);
});

app.get('/api/getHandler/:uid',(req,resp,next)=>{
     handler.getHandler(req,resp);
});

app.post('/api/saveHandler',(req,resp,next)=>{

    handler.saveHandler(req,resp);
});


app.get('/api/getById/:taskId',(req,resp,next)=>{
     tasks.getById(req,resp);
});

app.post('/api/updateEvents/:eid',(req,resp,next)=>{
       updateEvent.updateEvents(req,resp);
});
