const mysql = require('mysql2');
const db = require('./db/db');
const date = require('./common');
var connection = db.getDBConnection();
connection.connect();

var saveTask =(req,resp)=>{
    var connection = db.getDBConnection();
    connection.connect();
    var taskName = req.body.taskName;
    var bucket_id_fk = req.body.boardId;
    var event_id_fk = req.body.event_id_fk;
    var created_by_fk = req.body.created_by_fk;
    var created_on = date.getCurrentDateandTime();


    var save_qry = `INSERT INTO dat_tasks(taskname,bucket_id_fk,event_id_fk, created_on, created_by_fk ) VALUES ('${taskName}',${bucket_id_fk},${event_id_fk} ,'${created_on}', '${created_by_fk}')`;
    console.log(save_qry);
    connection.query(save_qry ,  function(err, results) {

        if (err) {
            console.log(err);
            resp.json({
                status: "Error",
                message:"Not Saved"
            });

        } else {
            resp.status(200).json({
                status: "SUCCESS",
                message:"saved successfully",
                data:results
            });

        }

    });
} //saveContainer

var getTasks = (req,resp) => {

boardId = req.params.boardId;
console.log('boardId'+boardId);
   var checkQry = `select t1.id,t1.taskname,t1.bucket_id_fk,t1.event_id_fk,t2.hname from handler.dat_tasks t1 inner join handler.dat_handler t2 on t1.bucket_id_fk=t2.id where t1.bucket_id_fk
 = ${boardId}`;
   // console.log("checkQry is "+ checkQry);
   connection.query(checkQry , function(err,result) {

    if (err) {
    	console.log("err is "+ err);
    }
    else
    {
    	if(result.length > 0)
    	{
    		resp.status(200).json({
    		status : "success",
    		data : result
    	   });
    	}
    	else
    	{
            resp.json({
    		status : "No data found"
    	   });
    	}


    }

   });

}

var getById = (req,resp) => {
  var taskId = req.params.taskId;

     var getTaskQry = `select id,taskname as taskName from handler.dat_tasks where id = ${taskId}`;
    console.log("getTaskQry is "+ getTaskQry);
     connection.query(getTaskQry , function(err,result)  {

      if (err) {
      	console.log("err is "+ err);
      }
      else
      {
      	if(result.length > 0)
      	{
      		resp.status(200).json({
      		status : "success",
      		data : result
      	   });
      	}
      	else
      	{
              resp.json({
      		status : "No data found"
      	   });
      	}


      }

     });
}





var updateTask = (req,resp) => {
  var taskId = req.params.taskId;
  console.log(req.params.taskId);
var taskname = req.body.taskName;
console.log(req.body.taskName);
     var updateTaskQry = `update dat_tasks set taskname = '${taskname}' where id = ${taskId}`;
     console.log(updateTaskQry);
     connection.query(updateTaskQry, function(err,result){
       if(err){
         console.log(err);
       }
       else{
         console.log(result);
         resp.status(200).json({
         status : "updated",
         data : result
          });
       }
     });
}





var getEvents = (req,resp) => {

 var checkQry = `select id,ename from mas_events`;
   // console.log("checkQry is "+ checkQry);
   // connection.query(checkQry , function(err,result) {
   connection.query(checkQry, function(err,result){
    if (err) {
    	console.log("err is "+ err);
    }
    else
    {
    	if(result.length > 0)
    	{
    		resp.status(200).json({
    		status : "success",
    		data : result
    	   });
    	}
    	else
    	{
            resp.json({
    		status : "No data found"
    	   });
    	}


    }

   });

}

module.exports={saveTask,getTasks,getEvents,updateTask,getById};
