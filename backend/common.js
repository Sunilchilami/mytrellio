const moment=require('moment-timezone');
var getCurrentDateandTime=()=>{
    var curr_moment=moment().tz("Asia/Kolkata").format('YYYY-MM-DD HH:mm:ss');
    return curr_moment;

} //getCurrentDateandTime

module.exports={getCurrentDateandTime};
