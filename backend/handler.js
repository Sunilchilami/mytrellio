const mysql = require('mysql2');
const db = require('./db/db');
const date = require('./common');

var connection = db.getDBConnection();
connection.connect();

var saveHandler=(req,resp)=>{
    var connection = db.getDBConnection();
    connection.connect();
    var hname = req.body.hname;
    // var container_id_fk = req.body.container_id_fk;
    var container_id_fk = 1;
    var created_by_fk = req.body.created_by_fk;
    var created_on = date.getCurrentDateandTime();


    var save_qry = `INSERT INTO dat_handler(hname,container_id_fk, created_on, created_by_fk ) VALUES ('${hname}' ,${container_id_fk} ,'${created_on}', '${created_by_fk}')`;
    console.log(save_qry);
    connection.query(save_qry ,  function(err, results) {

        if (err) {
            console.log(err);
            resp.json({
                status: "Error",
                message:"Not Saved"
            });

        } else {
            resp.status(200).json({
                status: "SUCCESS",
                message:"saved successfully",
                data:results
            });

        }

    });
} //saveHandler

var getHandler = (req,resp) => {

var uid = req.params.uid;

   var getHandlerQry = `select * from dat_handler where created_by_fk = ${uid}`;
  console.log("getHandlerQry is "+ getHandlerQry);
   connection.query(getHandlerQry , function(err,result) {

    if (err) {
    	console.log("err is "+ err);
    }
    else
    {
    	if(result.length > 0)
    	{
    		resp.status(200).json({
    		status : "success",
    		data : result
    	   });
    	}
    	else
    	{
            resp.json({
    		status : "No data found"
    	   });
    	}


    }

   });

}//getHandler


module.exports={saveHandler,getHandler};
