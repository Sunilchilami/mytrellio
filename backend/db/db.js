const mysql = require('mysql2');
const config_data = require('../dbconfig/config.json');


var getDBConnection=()=>{
    // create the connection to database
    const connection = mysql.createConnection({
        host: config_data.host,
        user: config_data.user,
        password : config_data.password,
        database: config_data.database
    });

    return connection;
}

var getCurrentDateandTime=()=>{
    var curr_moment=moment().tz("Asia/Kolkata").format('YYYY-MM-DD HH:mm:ss');
    return curr_moment;
}

module.exports={getDBConnection , getCurrentDateandTime};
    